# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary : Technical test application to display LeBonCoin catalog
* Version 1.0
* Development Stack : Kotlin, Jetpack, MVVM, Repository pattern, Room, WorkManager, Glide, Moshi, Retrofit, Coroutines, Timber ...

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin : mohamednour.bghouri@gmail.com
* Other community or team contact