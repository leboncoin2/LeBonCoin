package com.leboncoin.catalog

import com.leboncoin.catalog.database.AlbumDaoTest
import com.leboncoin.catalog.ui.AlbumsListFragmentTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.runner.RunWith
import org.junit.runners.Suite

@ExperimentalCoroutinesApi
@RunWith(Suite::class)
// @formatter:off
@Suite.SuiteClasses(
    ExampleInstrumentedTest::class,
    AlbumDaoTest::class,
    AlbumsListFragmentTest::class
)
// @formatter:on
class AllInstrumentedTests {
}