package com.leboncoin.catalog.repository

import androidx.lifecycle.MutableLiveData
import com.leboncoin.catalog.data.repository.IRepository
import com.leboncoin.catalog.domain.pojo.Album

/**
 * Implementation of a remote data source with static access to the data for easy testing.
 */
class FakeAndroidTestRepository() : IRepository {

    override val albums = MutableLiveData<List<Album>>()
    override suspend fun refreshAlbums() {
    }

    fun addAlbum(album: Album) {
        val list = if (albums.value == null) mutableListOf() else albums.value!!.toMutableList()
        list.add(album)
        albums.value = list
    }
}
