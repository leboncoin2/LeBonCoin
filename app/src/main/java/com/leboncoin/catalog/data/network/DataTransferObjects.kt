package com.leboncoin.catalog.data.network

import com.leboncoin.catalog.database.DatabaseAlbum
import com.leboncoin.catalog.domain.pojo.Album
import com.squareup.moshi.JsonClass

/**
 * AlbumsHolder holds a list of Albums.
 *
 * This is to parse first level of our network result which looks like
 *
 * {
 *   "albums": []
 * }
 */
@JsonClass(generateAdapter = true)
data class NetworkAlbumsContainer(val albums: List<Album>)

/**
 * Convert Network results to database objects
 */
fun NetworkAlbumsContainer.asDatabaseModel(): List<DatabaseAlbum> {
    return albums.map {
        DatabaseAlbum(
            id = it.id,
            albumId = it.albumId,
            title = it.title,
            url = it.url,
            thumbnailUrl = it.thumbnailUrl
        )
    }
}

