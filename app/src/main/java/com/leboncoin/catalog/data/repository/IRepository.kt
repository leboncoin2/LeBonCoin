package com.leboncoin.catalog.data.repository

import androidx.lifecycle.LiveData
import com.leboncoin.catalog.domain.pojo.Album

interface IRepository {
    val albums: LiveData<List<Album>>

    /**
     * Refresh the albums stored in the offline cache.
     */
    suspend fun refreshAlbums()
}