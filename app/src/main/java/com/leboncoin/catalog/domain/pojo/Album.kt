package com.leboncoin.catalog.domain.pojo

import com.squareup.moshi.JsonClass

/**
 * Album model data class.
 */
@JsonClass(generateAdapter = true)
data class Album(
    val id: Int,
    val albumId: Int,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)