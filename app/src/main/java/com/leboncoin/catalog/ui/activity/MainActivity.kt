package com.leboncoin.catalog.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.leboncoin.catalog.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}