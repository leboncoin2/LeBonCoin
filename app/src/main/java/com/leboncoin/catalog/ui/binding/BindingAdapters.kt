package com.leboncoin.catalog.ui.binding

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.RequestOptions
import com.leboncoin.catalog.R


/**
 * Binding adapter used to hide the spinner once data is available or no internet
 */
@BindingAdapter("isNetworkError", "datalist")
fun hideIfNetworkError(view: View, isNetWorkError: Boolean, dataList: Any?) {
    view.visibility = if (isNetWorkError || dataList != null) View.GONE else View.VISIBLE
}

@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        val updatedImgUrl = GlideUrl(
            imgUrl, LazyHeaders.Builder()
                .addHeader("User-Agent", "5")
                .build()
        )

        Glide.with(imgView.context).load(updatedImgUrl).apply(
            RequestOptions()
                .placeholder(R.drawable.loading_animation)
                .error(R.drawable.ic_broken_image)
        )
            .into(imgView)
    }
}
